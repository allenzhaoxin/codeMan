package codeMaker;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import constant.ChildWindowConstant;

/**
 * 常用参数配置类
 *
 * @author zrx
 */
public class CommonParamsConfig {

	private JFrame frame;

	public JFrame getFrame() {
		return frame;
	}

	/**
	 * Launch the application.
	 */
	public static void main() {
		EventQueue.invokeLater(() -> {
			try {
				CommonParamsConfig window = new CommonParamsConfig();
				ChildWindowConstant.commonParamsConfig = window;
				window.frame.setVisible(true);
			} catch (Exception ignored) {
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CommonParamsConfig() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("常用参数配置");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(CommonParamsConfig.class
				.getResource("/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/palette.png")));
		frame.setBounds(100, 100, 410, 257);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JLabel databasePoolLable = new JLabel("数据库连接池");

		JComboBox<String> databasePoolComboBox = new JComboBox<>();
		databasePoolComboBox.setModel(new DefaultComboBoxModel<>(new String[]{"Druid", "HikariCP"}));

		databasePoolComboBox.setSelectedItem(ChildWindowConstant.commonParametersModel.getDatabasePool());

		JLabel swaggerLable = new JLabel("整合Swagger");

		JComboBox<String> swaggerComboBox = new JComboBox<>();
		swaggerComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"是", "否"}));

		swaggerComboBox.setSelectedItem(ChildWindowConstant.commonParametersModel.getIfUseSwagger());

		JLabel label = new JLabel("更多参数配置，开发中。。。。。。");

		JButton saveButton = new JButton("保存");

		/**
		 * 点击保存
		 */
		saveButton.addActionListener(e -> {

			String databasePool = (String) databasePoolComboBox.getSelectedItem();
			String ifUseSwagger = (String) swaggerComboBox.getSelectedItem();

			ChildWindowConstant.commonParametersModel.setDatabasePool(databasePool);
			ChildWindowConstant.commonParametersModel.setIfUseSwagger(ifUseSwagger);

			JOptionPane.showMessageDialog(frame, "保存成功！", "提示", JOptionPane.INFORMATION_MESSAGE);

		});

		/**
		 * 布局设置
		 */
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addContainerGap()
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
																.addGroup(groupLayout.createSequentialGroup()
																		.addComponent(swaggerLable)
																		.addPreferredGap(ComponentPlacement.UNRELATED)
																		.addComponent(swaggerComboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
																		.addComponent(databasePoolLable)
																		.addPreferredGap(ComponentPlacement.RELATED)
																		.addComponent(databasePoolComboBox, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)))
														.addComponent(label)))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(163)
												.addComponent(saveButton)))
								.addContainerGap(174, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addContainerGap()
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(databasePoolLable)
										.addComponent(databasePoolComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(18)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(swaggerLable)
										.addComponent(swaggerComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(33)
								.addComponent(label)
								.addGap(29)
								.addComponent(saveButton)
								.addContainerGap(49, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
