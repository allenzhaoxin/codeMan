package ${packageName}.entity;
<#if isAuthority>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import ${packageName}.core.annotation.ExternalField;
import ${packageName}.core.validates.Add;
import ${packageName}.core.validates.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;


@ApiModel
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class CmSysUserEntity extends CommonEntity implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * user_id
	 */
	@ApiModelProperty(value = "user_id", name = "userId")
	private Long userId;
	/**
	 * role_id
	 */
	@ApiModelProperty(value = "role_id", name = "roleId")
	@NotNull(message = "角色不能为空！", groups = {Add.class, Update.class})
	private Long roleId;
	/**
	 * username
	 */
	@ApiModelProperty(value = "username", name = "username")
	@NotBlank(message = "请输入用户名！", groups = {Add.class, Update.class})
	private String username;
	/**
	 * password
	 */
	@ApiModelProperty(value = "password", name = "password")
	@NotBlank(message = "请输入密码！", groups = {Add.class})
	private String password;
	/**
	 * create_time
	 */
	@ApiModelProperty(value = "create_time", name = "createTime")
	private Date createTime;
	/**
	 * update_time
	 */
	@ApiModelProperty(value = "update_time", name = "updateTime")
	private Date updateTime;
	/**
	 * create_user_id
	 */
	@ApiModelProperty(value = "create_user_id", name = "createUserId")
	private Long createUserId;
	/**
	 * update_user_id
	 */
	@ApiModelProperty(value = "update_user_id", name = "updateUserId")
	private Long updateUserId;

	/**
	 * 角色名称
	 */
	@ApiModelProperty(value = "角色名称", name = "roleName")
	@ExternalField
	private String roleName;

	@ApiModelProperty(value = "该用户拥有的菜单")
	@ExternalField
	private List<CmSysMenuEntity> menus;

	@ApiModelProperty(value = "该用户拥有的按钮")
	@ExternalField
	private Map<Long, List<CmSysButtonEntity>> buttons;
}
<#else>
<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

<#if ifUseSwagger == "是">
@ApiModel
</#if>
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
	
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "用户名", name = "${userNameFiled}")
	</#if>
	private String ${userNameFiled};

	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "密码", name = "${userPwdFiled}")
	</#if>
	private String ${userPwdFiled};
}
</#if>
