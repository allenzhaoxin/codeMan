package ${packageName}.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.validation.annotation.Validated;
import ${packageName}.constant.YesOrNo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ${packageName}.dto.NodeMoveDto;
import ${packageName}.entity.CmSysUserEntity;
import ${packageName}.service.CmSysMenuService;
import ${packageName}.core.annotation.LoginRequired;
import ${packageName}.core.annotation.RecordLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import ${packageName}.entity.CmSysMenuEntity;
import ${packageName}.utils.SessionUtil;
import ${packageName}.utils.SnowflakeIdWorker;

import java.util.Date;
import java.util.List;

@RestController
@Api(tags = "cmSysMenu接口")
@RequestMapping("/cmSysMenu")
public class CmSysMenuController {


	private final CmSysMenuService service;

	@Autowired
	public CmSysMenuController(CmSysMenuService service) {
		this.service = service;
	}

	/**
	 * 查询全部节点
	 *
	 * @return
	 */
	@ApiOperation(value = "查询全部节点")
	@LoginRequired
	@RecordLog
	@GetMapping(value = "/list-all")
	public List<CmSysMenuEntity> listAll(Long roleId, @RequestParam(defaultValue = "NO") YesOrNo isAll) {
		return service.listAll(roleId, isAll);
	}

	/**
	 * 查询全部节点
	 *
	 * @return
	 */
	@ApiOperation(value = "移动节点")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/remove-node")
	public CmSysMenuEntity removeNode(@RequestBody NodeMoveDto nodeMoveDto) {
		return service.removeNode(nodeMoveDto);
	}

	/**
	 * 查看节点是否可移动
	 *
	 * @return
	 */
	@ApiOperation(value = "查看节点是否可移动")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/can-move")
	public boolean canMove(@RequestBody NodeMoveDto nodeMoveDto) {
		return service.canMove(nodeMoveDto);
	}

	/**
	 * 更新
	 *
	 * @return
	 */
	@ApiOperation(value = "更新")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/update")
	public void update(@RequestBody @Validated CmSysMenuEntity entity, HttpServletRequest request) {
		CmSysUserEntity user = SessionUtil.getUser(request);
		entity.setUpdateTime(new Date());
		entity.setUpdateUserId(user.getUserId());
		service.update(entity);
	}

	/**
	 * 添加
	 *
	 * @return
	 */
	@ApiOperation(value = "添加")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/add")
	public CmSysMenuEntity add(@RequestBody @Validated CmSysMenuEntity entity, HttpServletRequest request) {
		CmSysUserEntity user = SessionUtil.getUser(request);
		entity.setMenuId(SnowflakeIdWorker.generateId());
		entity.setOrderNo(entity.getMenuId());
		//前台用
		entity.setName(entity.getMenuName());
		entity.setCreateTime(new Date());
		entity.setCreateUserId(user.getUserId());
		service.add(entity);
		return entity;
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	@ApiOperation(value = "删除")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/delete")
	public void delete(@RequestBody CmSysMenuEntity entity) {
		service.delete(entity);
	}

}
