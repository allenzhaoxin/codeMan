package ${packageName}.config;

<#if frameWorkVal=="springBoot">
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * @author zrx json long转string
 */
@Configuration
public class JacksonConfig {

	@Bean
	@Primary
	@ConditionalOnMissingBean(ObjectMapper.class)
	public ObjectMapper jacksonObjectMapper(Jackson2ObjectMapperBuilder builder) {
		ObjectMapper objectMapper = builder.createXmlMapper(false).build();
		// 全局配置序列化返回 JSON 处理
		SimpleModule simpleModule = new SimpleModule();
		//JSON Long ==> String
		simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
		objectMapper.registerModule(simpleModule);
		return objectMapper;
	}
}
<#else>
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;

public class JacksonConfig extends ObjectMapper {

	public JacksonConfig() {
		super();
		// 设置日期转换yyyy-MM-dd HH:mm:ss
		setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		SimpleModule simpleModule = new SimpleModule();
		simpleModule.addSerializer(BigDecimal.class,
				ToStringSerializer.instance);
		simpleModule.addSerializer(BigInteger.class,
				ToStringSerializer.instance);
		simpleModule.addSerializer(Long.class,
				ToStringSerializer.instance);
		simpleModule.addSerializer(Long.TYPE,
				ToStringSerializer.instance);
		registerModule(simpleModule);
	}
}
</#if>
