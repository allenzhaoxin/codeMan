package ${packageName}.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ${packageName}.dao.CmSysButtonDao;
import ${packageName}.entity.CmSysButtonEntity;
import ${packageName}.service.CmSysButtonService;

@Service
public class CmSysButtonServiceImpl implements CmSysButtonService {

	
	private final CmSysButtonDao dao;

	@Autowired
	public CmSysButtonServiceImpl(CmSysButtonDao dao) {
		this.dao = dao;
	}
	
	@Override
	public void add(CmSysButtonEntity entity) {
		dao.add(entity);
	}
	
	@Override
	public void delete(CmSysButtonEntity entity) {
		dao.delete(entity);
	}
	
	@Override
	public void update(CmSysButtonEntity entity) {
		dao.update(entity);
	}
	
}
