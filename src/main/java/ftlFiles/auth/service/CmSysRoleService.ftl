package ${packageName}.service;

import ${packageName}.entity.CmSysButtonEntity;
import ${packageName}.entity.CmSysMenuEntity;
import ${packageName}.entity.CmSysRoleEntity;
import ${packageName}.entity.PageData;

import java.util.List;
import java.util.Map;

/**
 * @author zrx
 */
public interface CmSysRoleService {

	void add(CmSysRoleEntity entity);

	void delete(CmSysRoleEntity entity);
	
	void update(CmSysRoleEntity entity);
	
	PageData<CmSysRoleEntity> likeSelect(CmSysRoleEntity entity);

	/**
	 * 查询全部
	 * @return
	 */
	List<CmSysRoleEntity> listAll();

	/**
	 * 根据id查询
	 * @param roleId
	 * @return
	 */
	CmSysRoleEntity getById(Long roleId);

	/**
	 * 根据id获取角色拥有的菜单
	 * @param roleId
	 * @return
	 */
	List<CmSysMenuEntity> getMenusById(Long roleId);

	/**
	 * 根据id获取角色拥有的按钮
	 * @param roleId
	 * @return
	 */
	Map<Long, List<CmSysButtonEntity>> getButtonsById(Long roleId);
}
